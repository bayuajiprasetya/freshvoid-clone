import { createTheme, ThemeProvider } from "@material-ui/core";
import React from "react";
import { Route, Switch } from "react-router-dom";
import Help from "./pages/Help";
import Home from "./pages/Home";
import Transaction from "./pages/Transaction";
import Profile from "./pages/Profile";
import ProductDetail from "./details/ProductDetail";
import BannerDetail from "./details/BannerDetail";
import CategoryDetail from "./details/CategoryDetail";
import FeaturedItem from "./details/FeaturedItem";
import ScrollToTop from "./utils/ScrollToTop";
import { Poin } from "./details/Poin";
import { Voucher } from "./details/Voucher";

const theme = createTheme({
  typography: {
    fontFamily: ["Montserrat"].join(","),
  },
  overrides: {
    AutoplaySwipeableViews: {
      width: "calc(100% - 1.5rem)",
    },
  },
});

export default function App() {
  return (
    <ThemeProvider theme={theme}>
      <ScrollToTop>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/transaction" component={Transaction} />
          <Route path="/help" component={Help} />
          <Route path="/profile/vouchers" component={Voucher} />
          <Route path="/profile/point" component={Poin} />
          <Route path="/profile" component={Profile} />
          <Route path="/product/:id" component={ProductDetail} />
          <Route path="/banner/:id" component={BannerDetail} />
          <Route path="/category/:id" component={CategoryDetail} />
          <Route path="/top-seller" component={FeaturedItem} />
        </Switch>
      </ScrollToTop>
    </ThemeProvider>
  );
}
