import React from "react";
import {
  AppBar,
  Box,
  Container,
  IconButton,
  Toolbar,
  Typography,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { useHistory } from "react-router-dom";
import { ArrowBackIos } from "@material-ui/icons";
import InfiniteScroll from "react-infinite-scroll-component";

const useStyles = makeStyles({
  root: {
    height: "100vh",
    padding: 0,
    borderLeft: "1px solid #f1f1f1",
    paddingTop: 64,
    borderRight: "1px solid #f1f1f1",
    marginBottom: 0,
    maxWidth: 444,
    minHeight: "100vh",
    marginTop: 0,
    backgroundColor: "white",
  },

  Box: {
    display: "flex",
    top: 0,
    backgroundColor: "white",
    width: "100%",
    zIndex: 99,
    maxWidth: 442,
    position: "fixed",
  },

  AppBar: {
    top: 0,
    width: "100%",
    zIndex: 999,
    position: "fixed",
    maxWidth: 442,
    boxShadow: "0px 1px 5px rgb(0 0 0 / 5%)",
    maxHeight: 120,
  },

  IconButton: {
    color: "#00A739",
    marginRight: 0,
  },

  NavbarText: {
    flexGrow: 1,
    fontSize: 14,
    fontWeight: 600,
  },
});

const VoucherDetail = () => {
  const classes = useStyles();
  const history = useHistory();

  return (
    <>
      <Container maxWidth="xs" className={classes}>
        <Box className={classes.Box}>
          <AppBar
            className={classes.AppBar}
            position="static"
            color="primary"
            elevation={0}
            style={{ color: "black", backgroundColor: "white" }}
          >
            <Toolbar variant="dense" style={{ minHeight: 64 }}>
              <IconButton
                className={classes.IconButton}
                edge="start"
                aria-label="Menu"
                onClick={() => history.goBack()}
              >
                <ArrowBackIos />
              </IconButton>
              <Typography
                className={classes.NavbarText}
                variant="subtitle1"
                align="left"
              >
                Voucher Kamu
              </Typography>
            </Toolbar>
          </AppBar>
        </Box>
        <div style={{ marginTop: 10, padding: 16 }}>
          <InfiniteScroll></InfiniteScroll>
        </div>
      </Container>
    </>
  );
};

export default VoucherDetail;
