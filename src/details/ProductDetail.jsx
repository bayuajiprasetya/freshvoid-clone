import React, { useEffect, useState } from "react";
import {
  AppBar,
  Box,
  Button,
  CardContent,
  Container,
  Divider,
  Grid,
  IconButton,
  Paper,
  Toolbar,
  Typography,
  CircularProgress,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { ArrowBackIos } from "@material-ui/icons";
import { useHistory } from "react-router-dom";
import SetupAPI from "../AxiosInstance";
import clsx from "clsx";
import CurrencyFormatter from "../utils/CurrencyFormatter";
import SwipeableViews from "react-swipeable-views";

const useStyles = makeStyles({
  root: {
    height: "100vh",
    padding: 0,
    borderLeft: "1px solid #f1f1f1",
    paddingTop: 64,
    borderRight: "1px solid #f1f1f1",
    marginBottom: 0,
  },

  BoxOne: {
    top: 0,
    color: "white",
    width: "100%",
    zIndex: 99,
    position: "fixed",
    maxWidth: 442,
    background: "white",
  },

  BoxTwo: {
    display: "flex",
    justifyContent: "center",
  },

  AppBar: {
    top: 0,
    width: "100%",
    zIndex: 999,
    position: "fixed",
    maxWidth: 442,
    boxShadow: "0px 1px 5px rgb(0 0 0 / 5%)",
    maxHeight: 120,
  },

  IconButton: {
    color: "#00A739",
    marginRight: 0,
  },

  NavbarText: {
    flexGrow: 1,
    fontSize: 14,
    fontWeight: 600,
  },

  Paper: {
    height: "100%",
    maxWidth: 444,
    borderRadius: 0,
  },

  Grid: {
    padding: "0px 0px 24px 0px",
    marginTop: 70,
  },

  GridContain: {
    position: "relative",
    marginTop: -64,
  },

  Image: {
    color: "#000",
    padding: 8,
    marginRight: 20,
  },

  PaginationDot: {
    right: 0,
    width: "100%",
    bottom: 0,
    position: "absolute",
  },

  TitleItem: {
    color: "#252525",
    fontSize: 14,
    fontWeight: 600,
    marginBottom: 6,
  },

  PriceItem: {
    color: "#25282B",
    fontSize: 13,
    fontWeight: 700,
  },

  UnitItem: {
    color: "#25282B",
    fontSize: 10,
    marginTop: 3,
    marginBottom: 2,
  },
});

const ProductDetail = ({ match }) => {
  const classes = useStyles();
  const [items, setItem] = useState(null);
  const history = useHistory();
  const productId = match.params.id;
  const [isLoading, setIsLoading] = React.useState(true);

  useEffect(() => {
    const getData = async () => {
      setIsLoading(true);
      await SetupAPI.get(`/customer/ecommerce/products/${productId}`)
        .then((res) => {
          setItem(res.data.data);
        })
        .catch((err) => {
          console.log(err);
        })
        .finally(() => {
          setIsLoading(false);
        });
    };
    getData();
  }, [productId]);

  return (
    <>
      {isLoading ? (
        <div
          style={{
            display: "flex",
            alignItems: "center",
            flexDirection: "column",
            justifyContent: "center",
            minHeight: "100vh",
            margin: 0,
          }}
        >
          <CircularProgress
            style={{
              color: "#00A739",
            }}
          />
        </div>
      ) : (
        <>
          <Container className={classes.root} maxWidth="xs">
            <Box className={clsx(classes.BoxOne, classes.BoxTwo)} id="box">
              <AppBar
                className={classes.AppBar}
                position="static"
                color="primary"
                elevation={0}
                style={{ backgroundColor: "white", color: "black" }}
              >
                <Toolbar variant="dense" style={{ minHeight: 64 }}>
                  <IconButton
                    className={classes.IconButton}
                    edge="start"
                    aria-label="Menu"
                    onClick={() => history.goBack()}
                  >
                    <ArrowBackIos />
                  </IconButton>
                  <Typography
                    className={classes.NavbarText}
                    variant="subtitle1"
                    align="left"
                  >
                    Detail Produk
                  </Typography>
                </Toolbar>
              </AppBar>
            </Box>

            <Paper className={classes.Paper} elevation={0}>
              <div>
                <Grid className={classes.Grid}>
                  <Grid className={classes.GridContain}>
                    <div style={{ overflowX: "hidden" }}>
                      <SwipeableViews enableMouseEvents>
                        <div
                          className={classes.Image}
                          style={{
                            width: "100%",
                            background: `url(${items?.image?.url}) center center / cover no-repeat`,
                            height: 450,
                            borderRadius: 5,
                          }}
                        />
                      </SwipeableViews>
                    </div>
                    <div className={classes.PaginationDot}></div>
                  </Grid>
                </Grid>
              </div>
              <CardContent style={{ paddingBottom: 100 }}>
                <Grid container>
                  <Grid item={true} xs={12}>
                    <Typography
                      className={classes.TitleItem}
                      variant="caption"
                      display="block"
                    >
                      {items?.name}
                    </Typography>
                    <div style={{ display: "flex", alignItems: "center" }}>
                      <div style={{ display: "flex" }}>
                        <Typography
                          className={classes.PriceItem}
                          variant="caption"
                        >
                          {CurrencyFormatter.format(items?.price).replace(
                            "IDR",
                            ""
                          )}
                        </Typography>
                        <Typography
                          className={classes.UnitItem}
                          variant="caption"
                        >
                          /{items?.unit}
                        </Typography>
                      </div>
                    </div>
                  </Grid>
                  <Grid item={true} xs={12} style={{ marginTop: 8 }}>
                    <Divider style={{ marginBottom: 8 }} />
                    <Typography
                      variant="caption"
                      style={{ fontSize: 12, fontWeight: 700 }}
                    >
                      Deskripsi:
                    </Typography>
                  </Grid>
                  <Grid item={true} xs={12}>
                    <Typography variant="caption">
                      <div
                        style={{
                          fontSize: 10,
                          marginBottom: -2,
                          wordBreak: "break-word",
                        }}
                      >
                        <p
                          dangerouslySetInnerHTML={{
                            __html: items?.description,
                          }}
                        />
                        <p>&nbsp;</p>
                      </div>
                    </Typography>
                    <Divider style={{ marginBottom: 16 }} />
                  </Grid>
                  <div
                    style={{
                      width: "100%",
                      justifyContent: "center",
                      display: "flex",
                    }}
                  >
                    <Grid item={true} xs={4}>
                      <Button
                        variant="text"
                        style={{
                          color: "rgb(255, 255, 255)",
                          backgroundColor: "rgb(0, 167, 57)",
                          borderRadius: 5,
                          width: "100%",
                          height: 28,
                          fontSize: 13,
                          fontWeight: 700,
                          textTransform: "none",
                        }}
                      >
                        Beli
                      </Button>
                    </Grid>
                  </div>
                </Grid>
              </CardContent>
            </Paper>
          </Container>
        </>
      )}
    </>
  );
};

export default ProductDetail;
