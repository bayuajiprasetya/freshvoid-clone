import {
  AppBar,
  Box,
  Grid,
  IconButton,
  InputBase,
  makeStyles,
  SvgIcon,
  Toolbar,
  createTheme,
  ThemeProvider,
  Tabs,
  Tab,
  Paper,
} from "@material-ui/core";
import React, { useEffect } from "react";
import { Container } from "@material-ui/core";
import { ArrowBackIos } from "@material-ui/icons";
import SearchIcon from "@material-ui/icons/Search";
import clsx from "clsx";
import CategoryCardItem from "../components/CategoryCardItem";
import SetupAPI from ".././AxiosInstance";
import { useHistory } from "react-router-dom";

const theme = createTheme({
  overrides: {
    MuiTabs: {
      root: {},
      indicator: {
        backgroundColor: "#00A739",
      },
    },
    MuiTab: {
      root: {
        color: "rgb(0, 0, 0, 0.54)",
      },
      textColorPrimary: {
        "&$selected": {
          color: "#00A739",
        },
      },
    },
    MuiInputBase: {
      input: {
        width: "100%",
        padding: "8px 8px 8px 56px",
        transition: "width 300ms cubic-bezier(0.4, 0, 0.2, 1) 0ms",
        fontSize: 12,
        fontFamily: "Montserrat",
      },
    },
  },
});

const useStyles = makeStyles({
  root: {
    height: "100%",
    padding: 0,
    minHeight: "100vh",
    borderLeft: "1px solid #f1f1f1",
    borderRight: "1px solid #f1f1f1",
    backgroundColor: "#FAFAFA",
  },

  Paper: {
    paddingTop: 115,
    borderRadius: 0,
    backgroundColor: "#FAFAFA",
  },

  Grid: {
    width: "100%",
    zIndex: 99,
    position: "fixed",
    maxWidth: 442,
    boxShadow: "0px 2px 8px rgb(0 0 0 / 12%)",
    minHeight: 112,
    marginBottom: 10,
  },

  BoxOne: {
    display: "flex",
    justifyContent: "center",
  },

  BoxTwo: {
    top: 0,
    color: "white",
    zIndex: 99,
    position: "fixed",
    width: "100%",
    maxWidth: 442,
    background: "white",
  },

  AppBar: {
    top: 0,
    width: "100%",
    zIndex: 999,
    position: "fixed",
    maxWidth: 442,
    boxShadow: "0px 1px 5px rgb(0 0 0 / 5%)",
    maxHeight: 120,
  },

  IconButton: {
    color: "#00A739",
    marginRight: 0,
  },

  SearchProduct: {
    width: "100%",
    position: "relative",
    borderRadius: 50,
  },

  SearchIcon: {
    width: 56,
    height: "100%",
    display: "flex",
    position: "absolute",
    alignItems: "center",
    pointerEvents: "none",
    justifyContent: "center",
  },

  InputBase: {
    height: 40,
  },

  AppBarScrolled: {
    top: 64,
    width: "100%",
    padding: 0,
    position: "fixed",
    maxWidth: 442,
    marginBottom: 5,
  },

  Label: {
    fontSize: 14,
    fontWeight: 700,
    textTransform: "capitalize",
  },
});

function a11yProps(index) {
  return {
    id: `scrollable-auto-tab-${index}`,
    "aria-controls": `scrollable-auto-tabpanel-${index}`,
  };
}

const CategoryDetail = (props) => {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const [categories, setCategories] = React.useState([]);
  const history = useHistory();

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  useEffect(() => {
    const getData = async () => {
      await SetupAPI.get(
        `/customer/ecommerce/products/categories?page=1&perPage=100&isParent=true`
      )
        .then((res) => {
          setCategories(res.data.data);
        })
        .catch((err) => {
          console.log(err);
        });
    };
    getData();
  }, []);

  useEffect(() => {
    if (window.location.pathname === "/category/60ffeef340baba19ee4924b5") {
      setValue(0);
    } else if (
      window.location.pathname === "/category/60e46830a18eb7bde1b4c2c8"
    ) {
      setValue(1);
    } else if (
      window.location.pathname === "/category/60e46885a18eb7bde1b4c2ca"
    ) {
      setValue(2);
    } else if (
      window.location.pathname === "/category/60e468caa18eb7bde1b4c2cf"
    ) {
      setValue(3);
    } else if (
      window.location.pathname === "/category/60e46924a18eb7bde1b4c2d1"
    ) {
      setValue(4);
    }
  }, []);

  return (
    <ThemeProvider theme={theme}>
      <Container maxWidth="xs" className={classes.root}>
        <Grid className={classes.Grid} item={true} xs={12}>
          <Box className={clsx(classes.BoxOne, classes.BoxTwo)}>
            <AppBar
              className={classes.AppBar}
              position="static"
              color="primary"
              elevation={0}
              style={{ color: "black", backgroundColor: "white" }}
            >
              <Toolbar variant="dense" style={{ minHeight: 64 }}>
                <IconButton
                  className={classes.IconButton}
                  edge="start"
                  aria-label="Menu"
                  onClick={() => history.push("/")}
                >
                  <ArrowBackIos />
                </IconButton>
                <div
                  className={classes.SearchProduct}
                  style={{
                    marginLeft: 0,
                    backgroundColor: "rgb(242, 242, 242)",
                  }}
                >
                  <div className={classes.SearchIcon}>
                    <SvgIcon
                      focusable="false"
                      viewBox="0 0 24 24"
                      aria-hidden="true"
                      style={{ color: "rgb(112, 117, 133)" }}
                    >
                      <SearchIcon />
                    </SvgIcon>
                  </div>
                  <InputBase
                    className={classes.InputBase}
                    style={{ color: "rgb(112, 117, 133)" }}
                    placeholder="Cari Produk."
                  />
                </div>
              </Toolbar>
            </AppBar>
          </Box>
          <AppBar
            className={classes.AppBarScrolled}
            position="static"
            color="primary"
            elevation={0}
            style={{ color: "black", backgroundColor: "white" }}
          >
            <Tabs
              value={value}
              onChange={handleChange}
              variant="scrollable"
              textColor="primary"
              scrollButtons="on"
              aria-label="scrollable auto tabs example"
            >
              {categories.map((category, index) => (
                <Tab
                  className={classes.Label}
                  label={category.name}
                  {...a11yProps(value)}
                  key={category.id}
                  onClick={() => history.push(`/category/${category.id}`)}
                />
              ))}
            </Tabs>
          </AppBar>
        </Grid>
        <TabPanel value={value} index={value} />
      </Container>
    </ThemeProvider>
  );
};

export default CategoryDetail;

function TabPanel(props) {
  const { children, value, index, ...other } = props;
  const classes = useStyles();

  return (
    <>
      <Paper
        elevation={0}
        className={classes.Paper}
        hidden={value !== index}
        {...other}
      >
        {value === index && <CategoryCardItem index={index} />}
      </Paper>
    </>
  );
}
