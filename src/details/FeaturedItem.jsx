import React, { useState, useEffect } from "react";
import {
  Container,
  IconButton,
  makeStyles,
  Toolbar,
  Typography,
  Box,
  AppBar,
  Paper,
  Grid,
  CardMedia,
  Button,
  CircularProgress,
} from "@material-ui/core";
import { ArrowBackIos } from "@material-ui/icons";
import { useHistory } from "react-router-dom";
import InfiniteScroll from "react-infinite-scroll-component";
import SetupAPI from "../AxiosInstance";
import CurrencyFormatter from "../utils/CurrencyFormatter";

const useStyles = makeStyles({
  root: {
    height: "100%",
    padding: 0,
    borderLeft: "1px solid #f1f1f1",
    paddingTop: 64,
    borderRight: "1px solid #f1f1f1",
    marginBottom: 0,
    minHeight: "100vh",
    backgroundColor: "#FAFAFA",
  },

  BoxNavbar: {
    display: "flex",
    justifyContent: "center",
    color: "white",
    top: 0,
    width: "100%",
    zIndex: 99,
    position: "fixed",
    maxWidth: 442,
    background: "white",
  },

  AppBar: {
    top: 0,
    width: "100%",
    zIndex: 999,
    position: "fixed",
    maxWidth: 442,
    boxShadow: "0px 1px 5px rgb(0 0 0 / 5%)",
    maxHeight: 120,
  },

  IconButton: {
    color: "#00A739",
    marginRight: 0,
  },

  NavbarText: {
    flexGrow: 1,
    fontSize: 14,
    fontWeight: 600,
  },

  NavbarCaption: {
    color: "rgb(0, 167, 57)",
  },

  Paper: {
    borderRadius: 0,
  },

  BoxCard: {
    margin: "8px 16px",
    padding: "14px 11px",
    boxShadow: "0px 4px 8px rgb(0 0 0 /10%)",
    borderRadius: 10,
    backgroundColor: "#FFFFFF",
  },

  GridCard: {
    width: "100%",
    display: "flex",
    flexWrap: "wrap",
    boxSizing: "border-box",
  },

  GridImage: {
    width: 125,
    height: 125,
    display: "flex",
    borderRadius: 8,
  },

  Image: {
    width: "100%",
    height: "100%",
    margin: 0,
    display: "flex",
    flexDirection: "column",
  },

  Title: {
    color: "#252525",
    fontSize: 14,
    fontWeight: 600,
    width: "100%",
    display: "-webkit-box",
    WebkitLineClamp: 2,
    WebkitBoxOrient: "vertical",
    overflow: "hidden",
    height: "43px!important",
    margin: 0,
  },

  DiscountBadge: {
    width: "100%",
    height: "100%",
    margin: 0,
    display: "flex",
    flexDirection: "column",
  },

  DiscountText: {
    color: "#ffffff",
    width: "fit-content",
    margin: 0,
    opacity: 0.8,
    padding: 8,
    fontSize: 10,
    textAlign: "center",
    fontWeight: "bold",
    borderRadius: "5px 0px 5px 0px",
    backgroundColor: "#00A739",
  },

  PriceUnit: {
    height: 35,
  },

  CutOffPrice: {
    color: "#C7C7C9",
    textDecoration: "line-through",
  },

  Price: {
    fontSize: 13,
    fontWeight: 700,
  },

  Unit: {
    fontSize: 10,
    marginLeft: 2,
  },

  BuyButton: {
    width: 95,
    height: 28,
    padding: "5px 9px",
    fontSize: 13,
    marginTop: 17,
    borderRadius: 5,
    fontWeight: 700,
    textTransform: "none",
  },
});

const FeaturedItem = () => {
  const classes = useStyles();
  const history = useHistory();
  const [items, setItem] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    const getData = async () => {
      setIsLoading(true);
      await SetupAPI.get(`/customer/ecommerce/products?isFeatured=true&page=1`)
        .then((res) => {
          setItem(res.data.data);
        })
        .catch((err) => {
          console.log(err);
        })
        .finally(() => {
          setIsLoading(false);
        });
    };
    getData();
  }, []);

  return (
    <Container maxWidth="xs" className={classes.root}>
      <Box className={classes.BoxNavbar} id="box">
        <AppBar
          className={classes.AppBar}
          position="static"
          color="primary"
          elevation={0}
          style={{ backgroundColor: "white", color: "black" }}
        >
          <Toolbar variant="dense" style={{ minHeight: 64 }}>
            <IconButton
              className={classes.IconButton}
              edge="start"
              aria-label="Menu"
              onClick={() => history.goBack()}
            >
              <ArrowBackIos />
            </IconButton>
            <Typography
              className={classes.NavbarText}
              variant="subtitle1"
              align="left"
            >
              Produk Pilihan
            </Typography>
            <Typography className={classes.NavbarCaption} variant="caption">
              <strong></strong>
            </Typography>
          </Toolbar>
        </AppBar>
      </Box>
      {isLoading ? (
        <div
          style={{
            display: "flex",
            alignItems: "center",
            flexDirection: "column",
            justifyContent: "center",
            minHeight: "100vh",
            margin: 0,
          }}
        >
          <CircularProgress
            style={{
              color: "#00A739",
            }}
          />
        </div>
      ) : (
        <>
          <Paper elevation={0} className={classes.Paper}>
            <Grid container>
              <div style={{ width: "100%" }}>
                <InfiniteScroll
                  dataLength={{ item: Array.from({ length: 20 }) }}
                >
                  {items.map((item) => (
                    <Grid item={true} xs={12} key={item.id}>
                      <Box className={classes.BoxCard}>
                        <Grid className={classes.GridCard} container>
                          <Grid item={true} xs={4}>
                            <CardMedia
                              className={classes.GridImage}
                              style={{
                                filter: "unset",
                                backgroundImage: `url(${item.image.url})`,
                              }}
                            >
                              {item.isPromo === true ? (
                                <div
                                  className={classes.DiscountBadge}
                                  style={{ justifyContent: "flex-start" }}
                                >
                                  <p className={classes.DiscountText}>
                                    Disk.{" "}
                                    {Math.floor(
                                      (100 *
                                        (item.regularPrice - item.promoPrice)) /
                                        item.regularPrice
                                    )}
                                    %
                                  </p>
                                </div>
                              ) : (
                                <div className={classes.DiscountBadge} />
                              )}
                            </CardMedia>
                          </Grid>
                          <Grid item={true} xs={8} style={{ paddingLeft: 10 }}>
                            <p className={classes.Title}>{item.name}</p>
                            <div
                              style={{
                                fontSize: 10,
                                color: "rgb(204, 204, 204)",
                              }}
                            >
                              Min. order 1 x {item.unit}
                            </div>

                            <div className={classes.PriceUnit}>
                              {item.isPromo === true ? (
                                <Typography
                                  className={classes.CutOffPrice}
                                  variant="caption"
                                >
                                  {CurrencyFormatter.format(
                                    item.regularPrice
                                  ).replace("IDR", "")}
                                </Typography>
                              ) : null}
                              <div
                                style={{
                                  display: "flex",
                                  alignItems: "center",
                                }}
                              >
                                <Typography
                                  className={classes.Price}
                                  variant="body1"
                                >
                                  {CurrencyFormatter.format(item.price).replace(
                                    "IDR",
                                    ""
                                  )}
                                </Typography>
                                <Typography
                                  className={classes.Unit}
                                  variant="body1"
                                >
                                  /{item.unit}
                                </Typography>
                              </div>
                            </div>
                            <div
                              style={{
                                display: "flex",
                                alignItems: "flex-end",
                                justifyContent: "flex-end",
                              }}
                            >
                              <div>
                                <Button
                                  className={classes.BuyButton}
                                  variant="text"
                                  style={{
                                    color: "rgb(255, 255, 255)",
                                    backgroundColor: "rgb(0, 167, 57)",
                                  }}
                                >
                                  Beli
                                </Button>
                              </div>
                            </div>
                          </Grid>
                        </Grid>
                      </Box>
                    </Grid>
                  ))}
                  <p style={{ textAlign: "center" }}>
                    <strong>Yay! You have seen it all</strong>
                  </p>
                </InfiniteScroll>
              </div>
            </Grid>
          </Paper>
        </>
      )}
    </Container>
  );
};

export default FeaturedItem;
