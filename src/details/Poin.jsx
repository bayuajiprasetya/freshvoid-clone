import {
  Container,
  Box,
  AppBar,
  Toolbar,
  IconButton,
  Typography,
} from "@material-ui/core";
import { ArrowBackIos } from "@material-ui/icons";
import { makeStyles } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import React from "react";

const useStyles = makeStyles({
  root: {
    height: "100%",
    padding: 0,
    borderLeft: "1px solid #f1f1f1",
    paddingTop: 64,
    borderRight: "1px solid #f1f1f1",
    marginBottom: 0,
    minHeight: "100vh",
    backgroundColor: "#FAFAFA",
  },

  BoxNavbar: {
    display: "flex",
    justifyContent: "center",
    color: "white",
    top: 0,
    width: "100%",
    zIndex: 99,
    position: "fixed",
    maxWidth: 442,
    background: "white",
  },

  AppBar: {
    top: 0,
    width: "100%",
    zIndex: 999,
    position: "fixed",
    maxWidth: 442,
    boxShadow: "0px 1px 5px rgb(0 0 0 / 5%)",
    maxHeight: 120,
  },

  IconButton: {
    color: "#00A739",
    marginRight: 0,
  },

  NavbarText: {
    flexGrow: 1,
    fontSize: 14,
    fontWeight: 600,
  },

  NavbarCaption: {
    color: "rgb(0, 167, 57)",
  },

  VoucherCard: {
    border: "1px solid #F5F5F5",
    padding: 16,
    borderRadius: 8,
  },

  VoucherTitle: {
    fontSize: 14,
    fontWeight: 600,
  },

  UseButton: {
    color: "white",
    cursor: "pinter",
    height: "fit-content",
    padding: "4px 8px",
    fontSize: 12,
    fontWeight: 600,
    textAlign: "center",
    alignItems: "center",
    borderRadius: 4,
    backgroundColor: "#00A739",
  },
});

export const Poin = () => {
  const classes = useStyles();
  const history = useHistory();

  return (
    <>
      <Container className={classes.root}>
        <Box className={classes.BoxNavbar} id="box">
          <AppBar
            className={classes.AppBar}
            position="static"
            color="primary"
            elevation={0}
            style={{ backgroundColor: "white", color: "black" }}
          >
            <Toolbar variant="dense" style={{ minHeight: 64 }}>
              <IconButton
                className={classes.IconButton}
                edge="start"
                aria-label="Menu"
                onClick={() => history.goBack()}
              >
                <ArrowBackIos />
              </IconButton>
              <Typography
                className={classes.NavbarText}
                variant="subtitle1"
                align="left"
              >
                Poin Kamu
              </Typography>
              <Typography className={classes.NavbarCaption} variant="caption">
                <strong></strong>
              </Typography>
            </Toolbar>
          </AppBar>
        </Box>
        {/* <div style={{ marginTop: 10, padding: 16 }}>
            <InfiniteScroll dataLength={{ item: Array.from({ length: 20 }) }}>
              {Poins.map((voucher) => (
                <div style={{ marginBottom: 16 }}>
                  <div className={classes.VoucherCard}>
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "space-between",
                        alignItems: "center",
                      }}
                    >
                      <div style={{ width: "70%" }}>
                        <Typography
                          variant="body1"
                          className={classes.VoucherTitle}
                        ></Typography>
                      </div>
                      <div>
                        <div className={classes.UseButton}>Pakai</div>
                      </div>
                    </div>
                    <div>
                      <Divider style={{ margin: "10px 0px" }} />
                    </div>
                    <div>
                      <div
                        style={{
                          display: "flex",
                          alignItems: "center",
                          marginTop: 5,
                        }}
                      >
                        <WalletFIllIcon />
                        <div
                          style={{
                            fontSize: 10,
                            fontWeight: 500,
                            color: "rgb(128, 128, 128)",
                            marginLeft: 5,
                          }}
                        >
                          Minimum order{" "}
                          {CurrencyFormatter.format(voucher.minimumAmout).replace(
                            "IDR",
                            ""
                          )}
                        </div>
                      </div>
                      <div
                        style={{
                          display: "flex",
                          alignItems: "center",
                          marginTop: 5,
                        }}
                      >
                        <ClockFIllIcon />
                        <div
                          style={{
                            fontSize: 10,
                            fontWeight: 500,
                            color: "rgb(128, 128, 128)",
                            marginLeft: 5,
                          }}
                        >
                          Berlaku sampai {DateFormatter.format(voucher.expiredAt)}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              ))}
            </InfiniteScroll>
          </div> */}
        <div
          style={{
            display: "flex",
            alignItems: "center",
            flexDirection: "column",
            justifyContent: "center",
            minHeight: "85vh",
            margin: 0,
            fontSize: 16,
            fontWeight: 500,
          }}
        >
          Tidak ada Poin,{" "}
          <strong
            style={{ color: "#00A739", cursor: "pointer" }}
            onClick={() => history.push("/")}
          >
            Mau Poin?
          </strong>
        </div>
      </Container>
    </>
  );
};
