import React, { useEffect, useState } from "react";
//Importing Libraries and Components
import {
  AppBar,
  Box,
  IconButton,
  makeStyles,
  Toolbar,
  Typography,
  CircularProgress,
} from "@material-ui/core";
import { ArrowBackIos } from "@material-ui/icons";
import { Container } from "@material-ui/core";
import clsx from "clsx";
import { useHistory } from "react-router-dom";
import SetupAPI from "../AxiosInstance";

const useStyles = makeStyles({
  root: {
    height: "100%",
    padding: 0,
    borderLeft: "1px solid #f1f1f1",
    maxWidth: 444,
    minHeight: "100%",
    borderRight: "1px solid #f1f1f1",
    marginBottom: 0,
  },

  BoxOne: {
    top: 0,
    color: "white",
    width: "100%",
    zIndex: 99,
    position: "fixed",
    maxWidth: 442,
    background: "white",
  },

  BoxTwo: {
    display: "flex",
    justifyContent: "center",
  },

  AppBar: {
    top: 0,
    width: "100%",
    zIndex: 999,
    position: "fixed",
    maxWidth: 442,
    boxShadow: "0px 1px 5px rgb(0 0 0 / 5%)",
    maxHeight: 120,
  },

  IconButton: {
    color: "#00A739",
    marginRight: 0,
  },

  NavbarText: {
    flexGrow: 1,
    fontSize: 14,
    fontWeight: 600,
  },

  BoxContain: {
    padding: 16,
    marginTop: 64,
  },

  Image: {
    width: "100%",
  },

  DetailText: {
    fontSize: "1rem",
    fontFamily: "Montserrat",
    fontWeight: 400,
    lineHeight: 1.5,
  },
});

const BannerDetail = ({ match }) => {
  const classes = useStyles();
  const history = useHistory();
  const bannerId = match.params.id;
  const [banners, setBanner] = useState(null);
  const [isLoading, setIsLoading] = React.useState(true);

  useEffect(() => {
    const getData = async () => {
      setIsLoading(true);
      await SetupAPI.get(`/customer/ecommerce/banners/${bannerId}`)
        .then((res) => {
          setBanner(res.data.data);
        })
        .catch((err) => {
          console.log(err);
        })
        .finally(() => {
          setIsLoading(false);
        });
    };
    getData();
  }, [bannerId]);

  return (
    <>
      {isLoading ? (
        <div
          style={{
            display: "flex",
            alignItems: "center",
            flexDirection: "column",
            justifyContent: "center",
            minHeight: "100vh",
            margin: 0,
          }}
        >
          <CircularProgress
            style={{
              color: "#00A739",
            }}
          />
        </div>
      ) : (
        <Container maxWidth="xs" className={classes.root}>
          <Box className={clsx(classes.BoxOne, classes.BoxTwo)} id="box">
            <AppBar
              className={classes.AppBar}
              position="static"
              color="primary"
              elevation={0}
              style={{ backgroundColor: "white", color: "black" }}
            >
              <Toolbar variant="dense" style={{ minHeight: 64 }}>
                <IconButton
                  className={classes.IconButton}
                  edge="start"
                  aria-label="Menu"
                  onClick={() => history.goBack()}
                >
                  <ArrowBackIos />
                </IconButton>
                <Typography
                  className={classes.NavbarText}
                  variant="subtitle1"
                  align="left"
                >
                  {banners?.title}
                </Typography>
              </Toolbar>
            </AppBar>
          </Box>
          <Box className={classes.BoxContain}>
            <img
              className={classes.Image}
              src={banners?.image?.url}
              alt="banner"
            />
            <p
              className={classes.DetailText}
              dangerouslySetInnerHTML={{ __html: banners?.description }}
            />
          </Box>
        </Container>
      )}
    </>
  );
};

export default BannerDetail;
