import axios from "axios";

const instance = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
});

instance.interceptors.request.use((config) => {
  const token = localStorage.getItem("token");
  return {
    ...config,
    headers: {
      Authorization: "Bearer " + token,
      "x-tenant-id": process.env.REACT_APP_TENANT_ID,
      "X-location-id": process.env.REACT_APP_LOCATION_ID,
      ...config.headers,
    },
  };
});

instance.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    return Promise.reject(error);
  }
);

export default instance;
