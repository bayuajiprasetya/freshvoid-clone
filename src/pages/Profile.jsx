import React, { useEffect } from "react";
import {
  AppBar,
  Avatar,
  Button,
  Container,
  Divider,
  Grid,
  List,
  ListItem,
  makeStyles,
  Paper,
  Toolbar,
  Typography,
  CircularProgress,
} from "@material-ui/core";
import ContainsAccount from "../data/ListingAccount";
import ContainsInfo from "../data/ListingInfo";
import BottomNavigation from "../parts/Bottom";
import { ChangeProfileIcon, ArrowRightIcon } from "../Icon/Icons";
import { useHistory, useRouteMatch } from "react-router-dom";

const useStyles = makeStyles({
  root: {
    height: "100%",
    padding: 0,
    minHeight: "100vh",
    borderLeft: "1px solid #f1f1f1",
    paddingTop: 64,
    borderRight: "1px solid #f1f1f1",
    paddingBottom: 56,
    backgroundColor: "#FAFAFA",
  },

  AppBar: {
    top: 0,
    width: "100%",
    zIndex: 999,
    position: "fixed",
    maxWidth: 442,
    boxShadow: "0px 1px 5px rgb(0 0 0 / 5%)",
    maxHeight: 120,
    backgroundColor: "#FFFFFF",
  },

  GridToolbar: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },

  NavbarText: {
    color: "#14181B",
    fontFamily: "Montserrat",
  },

  ProfilBox: {
    width: "100%",
    padding: "24px 16px",
    maxWidth: 442,
    boxShadow: "0px 1px 5px rgb(0 0 0 / 5%)",
    paddingLeft: 16,
    borderRadius: 0,
    marginBottom: 5,
    paddingRight: 16,
    "@media (min-width: 600px)": {
      paddingLeft: 24,
      paddingRight: 24,
    },
  },

  Avatar: {
    width: 70,
    height: 70,
  },

  TextGroup: {
    color: "#14181B",
  },

  Logout: {
    color: "#EB4755",
    width: "100%",
    border: "1px solid #EB4755",
    cursor: "pointer",
    padding: "12px 0",
    fontSize: 14,
    textAlign: "center",
    fontWeight: 600,
    borderRadius: 8,
  },
});

export default function Profile() {
  const classes = useStyles();
  const [isLoading, setIsLoading] = React.useState(true);
  const history = useHistory();
  const { url, path } = useRouteMatch();

  useEffect(() => {
    setTimeout(() => {
      setIsLoading(false);
    }, 500);
  }, []);

  return (
    <>
      {isLoading ? (
        <div
          style={{
            display: "flex",
            alignItems: "center",
            flexDirection: "column",
            justifyContent: "center",
            minHeight: "100vh",
            margin: 0,
          }}
        >
          <CircularProgress
            style={{
              color: "#00A739",
            }}
          />
        </div>
      ) : (
        <>
          <Container maxWidth="xs" className={classes.root}>
            <AppBar
              position="static"
              elevation={0}
              color="primary"
              className={classes.AppBar}
            >
              <Toolbar>
                <Grid item={true} xs={1} className={classes.GridToolbar}>
                  <Typography variant="body1" className={classes.NavbarText}>
                    <b style={{ fontFamily: "Montserrat" }}>Profil</b>
                  </Typography>
                  <Button
                    variant="text"
                    style={{
                      color: "rgb(255, 255, 255)",
                      background: "rgb(255, 255, 255)",
                      textTransform: "none",
                      fontSize: 16,
                    }}
                  ></Button>
                </Grid>
              </Toolbar>
            </AppBar>
            <Paper
              elevation={0}
              className={classes.ProfilBox}
              style={{ backgroundColor: "rgb(255, 255, 255)" }}
            >
              <Grid container>
                <Grid
                  item={true}
                  xs={3}
                  align="center"
                  style={{ display: "flex" }}
                >
                  <Avatar
                    variant="circular"
                    className={classes.Avatar}
                    src="https://lh3.googleusercontent.com/a-/AOh14GgoduCaFLXdGxNaF9LnVArhw6LzydkOWPEDQKwH=s96-c"
                  />
                </Grid>
                <Grid
                  item={true}
                  xs={7}
                  style={{ display: "flex", alignItems: "center" }}
                >
                  <Typography
                    variant="body1"
                    align="left"
                    className={classes.TextGroup}
                    component={"div"}
                  >
                    <b style={{ fontSize: 16, fontWeight: 700 }}>
                      Bayu Aji Praseya
                    </b>
                    <p
                      style={{ fontSize: 12, fontWeight: 400, marginBottom: 0 }}
                    ></p>
                    <p
                      style={{
                        fontSize: 10,
                        color: "rgb(78, 83, 86)",
                        marginTop: 4,
                        marginBottom: 0,
                      }}
                    >
                      bayuaji.works@gmail.com
                    </p>
                  </Typography>
                </Grid>
                <Grid
                  item={true}
                  xs={2}
                  style={{
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "flex-end",
                  }}
                >
                  <ChangeProfileIcon />
                </Grid>
              </Grid>
            </Paper>
            <Paper
              elevation={0}
              style={{
                backgroundColor: "rgb(255, 255, 255)",
                marginTop: "3%",
                boxShadow: "rgb(0 0 0 / 5%) 0px 1px 5px",
              }}
            >
              <List component="nav">
                <Typography
                  variant="body1"
                  style={{
                    fontSize: 14,
                    fontWeight: 600,
                    padding: "10px 14px",
                  }}
                >
                  Akun
                </Typography>
                {ContainsAccount.map((item) => (
                  <div key={item.id}>
                    <ListItem button={true} style={{ padding: "20px 14px" }}>
                      {item.icon}
                      <div
                        style={{
                          color: "rgb(112, 117, 133)",
                          fontSize: 12,
                          fontWeight: 500,
                          marginLeft: 16,
                        }}
                        onClick={() => history.push(`${item.path}`)}
                      >
                        {item.text}
                      </div>
                      <div style={{ position: "absolute", right: 20 }}>
                        <ArrowRightIcon />
                      </div>
                    </ListItem>
                    {item.id !== ContainsAccount.length ? (
                      <Divider variant="middle" />
                    ) : null}
                  </div>
                ))}
              </List>
            </Paper>
            <Paper
              elevation={0}
              style={{
                backgroundColor: "rgb(255, 255, 255)",
                marginTop: "3%",
                boxShadow: "rgb(0 0 0 / 5%) 0px 1px 5px",
              }}
            >
              <List component="nav">
                <Typography
                  variant="body1"
                  style={{
                    fontSize: 14,
                    fontWeight: 600,
                    padding: "10px 14px",
                  }}
                >
                  Info Lainnya
                </Typography>
                {ContainsInfo.map((item, index) => (
                  <div key={index}>
                    <ListItem button={true} style={{ padding: "20px 14px" }}>
                      {item.icon}
                      <div
                        style={{
                          color: "rgb(112, 117, 133)",
                          fontSize: 12,
                          fontWeight: 500,
                          marginLeft: 16,
                        }}
                      >
                        {item.text}
                      </div>
                      <div style={{ position: "absolute", right: 20 }}>
                        <ArrowRightIcon />
                      </div>
                    </ListItem>
                    <Divider variant="middle" />
                  </div>
                ))}
                <div style={{ padding: "70px 16px 24px" }}>
                  <div className={classes.Logout}>Keluar</div>
                </div>
              </List>
            </Paper>
          </Container>
          <BottomNavigation />
        </>
      )}
    </>
  );
}
