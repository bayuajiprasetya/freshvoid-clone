import {
  AppBar,
  Box,
  CircularProgress,
  Container,
  Grid,
  IconButton,
  makeStyles,
  Paper,
  Toolbar,
  Typography,
} from "@material-ui/core";
import clsx from "clsx";
import React, { useEffect } from "react";
import BottomNavigation from "../parts/Bottom";

const useStyles = makeStyles({
  root: {
    height: "100%",
    padding: 0,
    minHeight: "100vh",
    borderLeft: "1px solid #f1f1f1",
    paddingTop: 64,
    borderRight: "1px solid #f1f1f1",
    paddingBottom: 56,
    backgroundColor: "white",
  },
  Wrapper: {
    flexGrow: 1,
  },

  BoxOne: {
    display: "flex",
    justifyContent: "center",
    backgroundColor: "white",
  },

  BoxTwo: {
    top: 0,
    color: "white",
    width: "100%",
    zIndex: 99,
    position: "fixed",
    maxWidth: 442,
    background: "white",
  },

  BoxThree: {
    top: 0,
    color: "white",
    width: "100%",
    zIndex: 99,
    position: "fixed",
    maxWidth: 442,
    background: "white",
  },

  Paper: {
    width: "100%",
    borderRadius: 0,
    backgroundColor: "white",
  },

  AppBar: {
    top: 0,
    width: "100%",
    zIndex: 999,
    position: "fixed",
    maxWidth: 442,
    boxShadow: "0px 1px 5px rgb(0 0 0 / 5%)",
    maxHeight: 120,
  },

  NavbarText: {
    flexGrow: 1,
    fontSize: 14,
    fontWeight: 600,
  },

  IconButton: {
    color: "#9FA3A6",
  },
});

export default function Transaction() {
  const classes = useStyles();
  const [isLoading, setIsLoading] = React.useState(true);

  useEffect(() => {
    setTimeout(() => {
      setIsLoading(false);
    }, 500);
  }, []);

  return (
    <>
      {isLoading ? (
        <div
          style={{
            display: "flex",
            alignItems: "center",
            flexDirection: "column",
            justifyContent: "center",
            minHeight: "100vh",
            margin: 0,
          }}
        >
          <CircularProgress
            style={{
              color: "#00A739",
            }}
          />
        </div>
      ) : (
        <>
          <Container maxWidth="xs" className={classes.root}>
            <div className={classes.Wrapper}>
              <Box
                className={clsx(
                  classes.BoxOne,
                  classes.BoxTwo,
                  classes.BoxThree
                )}
              >
                <AppBar
                  className={classes.AppBar}
                  position="static"
                  color="primary"
                  elevation={0}
                  style={{ backgroundColor: "white", color: "black" }}
                >
                  <Toolbar variant="dense" style={{ minHeight: 64 }}>
                    <Typography
                      className={classes.NavbarText}
                      variant="subtitle1"
                      align="left"
                    >
                      Transaksi
                    </Typography>
                    <IconButton edge="start" className={classes.IconButton}>
                      <img
                        src="https://freshvo.id/static/media/history.9c2878c5.svg"
                        alt="history"
                      />
                    </IconButton>
                  </Toolbar>
                </AppBar>
              </Box>
              <Paper className={classes.Paper} elevation={0}>
                <div align="center">
                  <Grid
                    container
                    style={{
                      flexDirection: "column",
                      display: "flex",
                      alignItems: "center",
                      paddingTop: 100,
                    }}
                  >
                    <img
                      src="https://freshvo.id/static/media/transaksikosong.3f0175bf.svg"
                      alt=""
                      style={{ marginBottom: 40 }}
                    />
                    <Typography
                      variant="subtitle1"
                      display="block"
                      gutterBottom={true}
                      style={{ fontSize: 16, fontWeight: 600 }}
                    >
                      <b>Transaksi Masih Kosong</b>
                    </Typography>
                    <Typography
                      variant="caption"
                      display="block"
                      gutterBottom={true}
                      style={{ fontSize: 12, fontWeight: 400 }}
                    >
                      Belum ada transaksi nih, belanja sekarang yuk!
                    </Typography>
                  </Grid>
                  <div style={{ backgroundColor: "rgb(250, 250, 250)" }}></div>
                </div>
              </Paper>
            </div>
          </Container>
          <BottomNavigation />
        </>
      )}
    </>
  );
}
