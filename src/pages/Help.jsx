import React, { useEffect } from "react";
import {
  AppBar,
  CircularProgress,
  Container,
  Divider,
  Toolbar,
  Typography,
} from "@material-ui/core";
import { Box, Grid, makeStyles } from "@material-ui/core";
import SetupAPI from "./../AxiosInstance";
import clsx from "clsx";
import BottomNavigation from "../parts/Bottom";
import { Link } from "react-router-dom";

const useStyles = makeStyles({
  root: {
    height: "100%",
    padding: 0,
    maxWidth: 444,
    minHeight: "100vh",
    borderLeft: "1px solid #f1f1f1",
    paddingTop: 64,
    borderRight: "1px solid #f1f1f1",
    paddingBottom: 56,
  },

  BoxOne: {
    display: "flex",
    justifyContent: "center",
  },

  BoxTwo: {
    top: 0,
    color: "white",
    width: "100%",
    zIndex: 99,
    position: "fixed",
    maxWidth: 442,
    background: "white",
  },

  AppBar: {
    top: 0,
    width: "100%",
    zIndex: 999,
    position: "fixed",
    maxWidth: 442,
    boxShadow: "0px 1px 5px rgb(0 0 0 / 5%)",
    maxHeight: 120,
  },

  NavbarText: {
    flexGrow: 1,
    fontSize: 14,
    fontWeight: 600,
  },

  GridItem: {
    padding: 16,
    background: "#FFFFFF",
    boxShadow: "0px 1px 5px rgb(0 0 0 / 5%)",
  },

  TitleItem: {
    fontSize: 12,
  },

  TextItem: {
    marginTop: 16,
  },

  Link: {
    color: "black",
    display: "flex",
    justifyContent: "space-between",
    textDecoration: "none",
  },

  Text: {
    fontSize: 12,
  },

  WAIcon: {
    color: "white",
    bottom: 16,
    position: "fixed",
  },
});

export default function Help() {
  const classes = useStyles();
  const [items, setItem] = React.useState([]);
  const [isLoading, setIsLoading] = React.useState(true);

  useEffect(() => {
    const getData = async () => {
      setIsLoading(true);
      await SetupAPI.get(
        `/customer/cms/faqs/categories?page=1&perPage=1000&search=`
      )
        .then((res) => {
          setItem(res.data.data);
        })
        .catch((err) => {
          console.log(err);
        })
        .finally(() => {
          setIsLoading(false);
        });
    };
    getData();
  }, []);

  return (
    <>
      {isLoading ? (
        <div
          style={{
            display: "flex",
            alignItems: "center",
            flexDirection: "column",
            justifyContent: "center",
            minHeight: "100vh",
            margin: 0,
          }}
        >
          <CircularProgress
            style={{
              color: "#00A739",
            }}
          />
        </div>
      ) : (
        <>
          <Container maxWidth="xs" className={classes.root}>
            <Box className={clsx(classes.BoxOne, classes.BoxTwo)}>
              <AppBar
                position="static"
                color="primary"
                className={classes.AppBar}
                elevation={0}
                style={{ backgroundColor: "white", color: "black" }}
              >
                <Toolbar variant="dense" style={{ minHeight: 64 }}>
                  <Typography
                    variatn="body1"
                    align="left"
                    className={classes.NavbarText}
                  >
                    Pusat Bantuan
                  </Typography>
                </Toolbar>
              </AppBar>
            </Box>
            <Grid
              elevation={0}
              align="center"
              style={{
                background: "rgb(0, 167, 57)",
                borderRadius: 0,
                color: "rgb(255, 255, 255)",
                opacity: 0.7,
                paddingTop: 20,
                paddingBottom: 20,
              }}
            >
              <Typography variant="h6" gutterBottom>
                <b>Kamu perlu bantuan?</b>
              </Typography>
            </Grid>
            {items.map((item, index) => (
              <Grid container className={classes.GridItem} key={index}>
                <Grid item={true} xs={12}>
                  <Typography variant="body1" className={classes.TitleItem}>
                    <b>{item.name}</b>
                  </Typography>
                  <Divider style={{ marginTop: "4%" }} />
                </Grid>
                {item.faqs.map((faq, faqIndex) => (
                  <Grid
                    item={true}
                    xs={12}
                    className={classes.TextItem}
                    key={faqIndex}
                  >
                    <Link className={classes.Link} to="">
                      <Typography className={classes.Text}>
                        {faq.title}
                      </Typography>
                    </Link>
                    <Divider style={{ marginTop: "4%" }} />
                  </Grid>
                ))}
              </Grid>
            ))}
            <Grid container className={classes.GridItem}>
              <Grid className={classes.TextItem} item={true} xs={12}>
                <Typography variant="body1" className={classes.Text}>
                  Masih <b>butuh bantuan</b> atau ada hal lain yang ingin
                  ditanyakan?
                  {""}
                  <Link
                    target="_blank"
                    rel="noreferrer"
                    to="https://api.whatsapp.com/send?phone=6281222001001&text=Hai%20Freshvo,%20saya%20ingin%20bertanya"
                    style={{
                      marginLeft: 6,
                      color: "rgb(0, 167, 57)",
                      textDecoration: "none",
                    }}
                  >
                    <b>HUBUNGI KAMI</b>
                  </Link>
                </Typography>
              </Grid>
            </Grid>
            <Grid container style={{ padding: 10 }}>
              <Grid
                item={true}
                xs={1}
                style={{
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <img
                  src="https://freshvo.id/static/media/info.f0b746a7.svg"
                  alt="Info"
                />
              </Grid>
              <Grid item={true} xs={11}>
                <Typography
                  variant="caption"
                  display="block"
                  gutterBottom
                  style={{ color: "rgb(137, 139, 140)", fontWeight: "bold" }}
                >
                  Layanan Pelanggan 24 Jam, Senin s/d Minggu, tidak termasuk
                  Hari Libur Nasional.
                </Typography>
              </Grid>
            </Grid>
            <div style={{ marginLeft: "74%" }}>
              <Link
                target="_blank"
                rel="noreferrer"
                to="https://api.whatsapp.com/send?phone=6281222001001&text=Hai%20Freshvo,%20saya%20ingin%20bertanya"
                style={{
                  color: "rgb(255, 255, 255)",
                  textDecoration: "none",
                }}
              >
                <img
                  src="https://freshvo.id/static/media/wa.4c0c2a92.svg"
                  alt="Whatsapp"
                  className={classes.WAIcon}
                  style={{ marginBottom: 56 }}
                />
              </Link>
            </div>
          </Container>
          <BottomNavigation />
        </>
      )}
    </>
  );
}
