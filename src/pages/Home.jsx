import { CircularProgress, makeStyles } from "@material-ui/core";
import Container from "@material-ui/core/Container";
import React, { useContext, useEffect } from "react";
import Navbar from "../parts/Navbar";
import InfoBox from "../parts/InfoBox";
import Carousel from "../parts/Carousel";
import Category from "../parts/Category";
import Card from "../parts/Card";
import BottomNavigation from "../parts/Bottom";
import Snackbar from "../components/Snackbar";
import { CartContext } from "../context/Cart";

const useStyles = makeStyles({
  root: {
    height: "100%",
    padding: 0,
    maxWidth: 444,
    minHeight: "100vh",
    borderLeft: "1px solid #f1f1f1",
    borderRight: "1px solid #f1f1f1",
    paddingBottom: 56,
    backgroundColor: "#FAFAFA",
  },
});

export default function Home() {
  const classes = useStyles();
  const { cart } = useContext(CartContext);
  const [isLoading, setIsLoading] = React.useState(true);

  useEffect(() => {
    setTimeout(() => {
      setIsLoading(false);
    }, 500);
  }, []);

  return (
    <>
      <Container
        maxWidth="xs"
        className={classes.root}
        style={{ marginBottom: cart.length > 0 ? 56 : null }}
        id="main"
      >
        {isLoading ? (
          <div
            style={{
              display: "flex",
              alignItems: "center",
              flexDirection: "column",
              justifyContent: "center",
              minHeight: "100vh",
              margin: 0,
            }}
          >
            <CircularProgress
              style={{
                color: "#00A739",
              }}
            />
          </div>
        ) : (
          <>
            <Navbar />
            <br />
            <InfoBox />
            <Carousel />
            <Category />
            <Card />
            <Snackbar />
          </>
        )}
      </Container>
      <BottomNavigation />
    </>
  );
}
