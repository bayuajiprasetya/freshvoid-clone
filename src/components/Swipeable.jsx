import { makeStyles } from "@material-ui/styles";
import React from "react";
import SwipeableViews from "react-swipeable-views";

const styles = {
  slide: {
    width: "100%",
    background:
      "url(https://warung-io.s3.ap-southeast-1.amazonaws.com/006_Buah%20Naga%20Merah.jpg-2021-07-21T10%3A37%3A25.549Z) center center / cover no-repeat",
    height: 450,
    borderRadius: 5,
  },
};

const useStyles = makeStyles({
  Image: {
    color: "#000",
    padding: 8,
    marginRight: 20,
  },
});

const Swipeable = () => {
  const classes = useStyles();
  return (
    <SwipeableViews enableMouseEvents>
      <div className={classes.Image} style={styles.slide} />
    </SwipeableViews>
  );
};

export default Swipeable;
