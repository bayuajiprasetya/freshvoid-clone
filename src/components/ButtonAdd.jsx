import React, { useContext } from "react";
import { CartContext } from "../context/Cart";
import { makeStyles, ButtonGroup, Button } from "@material-ui/core";

const styles = {
  BuyButtonStyles: {
    color: "rgb(255, 255, 255)",
    backgroundColor: "rgb(0, 167, 57)",
  },

  ButtonGroup: {
    justifyContent: "flex-end",
    width: "-webkit-fill-available",
    marginRight: 1,
  },

  DecreaseButton: {
    color: "rgb(0, 0, 0)",
    borderRadius: 5,
    maxWidth: 28,
    minWidth: 28,
    maxHeight: 28,
    minHeight: 28,
    border: "1px solid silver",
    fontWeight: 700,
  },

  IncreaseButton: {
    color: "rgb(255, 255, 255)",
    backgroundColor: "rgb(0, 167, 57)",
    borderRadius: 5,
    maxWidth: 28,
    minWidth: 28,
    maxHeight: 28,
    minHeight: 28,
    border: "1px solid rgb(0, 167, 57)",
    fontWeight: 700,
  },

  TotalButton: {
    border: 0,
    fontWeight: 800,
    fontSize: 13,
    maxWidth: 58,
    minWidth: 58,
    maxHeight: 28,
    minHeight: 28,
  },
};

const useStyles = makeStyles({
  BuyButton: {
    color: "#ffffff",
    width: "100%",
    height: 28,
    display: "flex",
    fontSize: "13px !important",
    alignItems: "center",
    fontFamily: `https://fonts.googleapis.com/css2?family=Open+Sans&display=swap`,
    fontWeight: 700,
    borderRadius: 5,
    textTransform: "none",
    justifyContent: "center",
    backgroundColor: "#00A739",
  },
});

const ButtonAdd = ({ data }) => {
  const { addCart, cart, increaseCart, decreaseCart } = useContext(CartContext);
  const classes = useStyles();
  const filteredCart = cart.filter((item) => item.id === data.id);

  return (
    <div>
      {filteredCart[0]?.total > 0 ? (
        <ButtonGroup style={styles.ButtonGroup}>
          <Button
            variant="outlined"
            size="small"
            style={styles.DecreaseButton}
            onClick={() => decreaseCart(data)}
          >
            -
          </Button>
          <Button variant="outlined" size="small" style={styles.TotalButton}>
            {filteredCart[0]?.total}
          </Button>
          <Button
            variant="outlined"
            size="small"
            style={styles.IncreaseButton}
            onClick={() => increaseCart(data)}
          >
            +
          </Button>
        </ButtonGroup>
      ) : (
        <Button
          onClick={() => addCart(data)}
          variant="text"
          className={classes.BuyButton}
          type="button"
          style={styles.BuyButtonStyles}
        >
          Beli
        </Button>
      )}
    </div>
  );
};

export default ButtonAdd;
