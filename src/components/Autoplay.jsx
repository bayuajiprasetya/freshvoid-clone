import React, { useEffect, useState } from "react";
import SwipeableViews from "react-swipeable-views";
import { autoPlay } from "react-swipeable-views-utils";
import Pagination from "./Pagination";
import { makeStyles } from "@material-ui/core";
import SetupAPI from "../AxiosInstance";

const AutoplaySwipeableViews = autoPlay(SwipeableViews);

const styles = {
  BannerPaperRadius: {
    display: "block",
    overflow: "hidden",
    borderRadius: 7,
    paddingRight: 16,
  },

  BannerImages: {
    width: "100%",
    maxWidth: 360,
    height: 150,
    filter: "unset",
  },
};

const useStyles = makeStyles({
  root: {
    position: "relative",
    flexGrow: 1,
  },

  BannerRange: {
    "& .react-swipeable-view-container": {
      width: "calc(100% - 1.5rem)",
    },
  },
});

function Autoplay() {
  const classes = useStyles();
  const [index, setIndex] = useState(0);
  const [banners, setBanners] = useState([]);

  useEffect(() => {
    const getData = async () => {
      await SetupAPI.get(`/customer/ecommerce/banners`)
        .then((res) => {
          setBanners(res.data.data);
        })
        .catch((err) => {
          console.log(err);
        });
    };
    getData();
  }, []);

  const handleStepChange = (e) => {
    setIndex(e);
  };

  return (
    <div className={classes.root}>
      <AutoplaySwipeableViews
        className={classes.BannerRange}
        index={index}
        onChangeIndex={handleStepChange}
      >
        {banners.map((banner, index) => (
          <div key={index}>
            <a href={`/banner/${banner.id}`}>
              <img
                src={banner.image.url}
                alt={banner.title}
                style={Object.assign(
                  {},
                  styles.BannerPaperRadius,
                  styles.BannerImages
                )}
              />
            </a>
          </div>
        ))}
      </AutoplaySwipeableViews>
      <Pagination dots={4} index={index} onChangeIndex={handleStepChange} />
    </div>
  );
}

export default Autoplay;
