import {
  Box,
  Button,
  CardMedia,
  createTheme,
  Grid,
  Typography,
  ThemeProvider,
  CircularProgress,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import React, { useEffect, useState } from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import SetupAPI from "./../AxiosInstance";
import CurrencyFormatter from "../utils/CurrencyFormatter";
import { useParams } from "react-router-dom";

const theme = createTheme({
  overrides: {
    MuiTypography: {
      body1: {
        fontFamily: "Montserrat",
        letterSpacing: null,
      },
    },
  },
});

const useStyles = makeStyles({
  GridContain: {
    width: "100%",
    display: "flex",
    flexWrap: "wrap",
    boxSizing: "border-box",
  },

  BoxCard: {
    margin: "8px 0px",
    padding: "14px 11px",
    boxShadow: "0px 4px 8px rgb(0 0 0 /10%)",
    borderRadius: 10,
    backgroundColor: "#FFFFFF",
  },

  GridCard: {
    "@media (max-width: 430px)": {
      minHeight: 95,
    },
    "@media (max-width: 320px)": {
      minHeight: 90,
    },
  },

  GridImage: {
    width: 125,
    height: 125,
    display: "flex",
    borderRadius: 8,
    "@media(max-width: 430px)": {
      width: 104,
      height: 104,
    },
    "@media(max-width: 320px)": {
      width: 90,
      height: 90,
    },
  },

  DiscountBadge: {
    width: "100%",
    height: "100%",
    margin: 0,
    display: "flex",
    flexDirection: "column",
  },

  DiscountText: {
    color: "#ffffff",
    width: "fit-content",
    margin: 0,
    opacity: 0.8,
    padding: 8,
    fontSize: 10,
    textAlign: "center",
    fontWeight: "bold",
    borderRadius: "5px 0px 5px 0px",
    backgroundColor: "#00A739",
  },

  Title: {
    color: "#252525",
    fontSize: 14,
    fontWeight: 600,
    width: "100%",
    display: "-webkit-box",
    WebkitLineClamp: 2,
    WebkitBoxOrient: "vertical",
    overflow: "hidden",
    margin: 0,
    height: "43px!important",
    "@media screen and (max-width: 430px)": {
      fontSize: 12,
      height: "40px !important",
    },
    "@media screen and (max-width: 375px)": {
      fontSize: 12,
      height: "37px !important",
    },
    "@media screen and (max-width: 320px)": {
      fontSize: 12,
      height: "35px !important",
      marginTop: 0,
    },
  },

  PriceUnit: {
    height: 35,
    display: "flex",
    flexDirection: "column",
    "@media (max-width: 320px)": {
      marginTop: -2,
    },
  },

  Price: {
    fontSize: 13,
    fontWeight: 700,
    "@media (max-width: 430px)": {
      fontSize: 11,
    },
    "@media (max-width: 320px)": {
      fontSize: 10,
    },
  },

  Unit: {
    fontSize: 10,
    marginLeft: 2,
    "@media (max-width: 430px)": {
      fontSize: 9,
    },
  },

  BuyButton: {
    width: 95,
    height: 28,
    padding: "5px 9px",
    fontSize: 13,
    marginTop: 17,
    borderRadius: 5,
    fontWeight: 700,
    textTransform: "none",
    "@media (max-width: 430px)": {
      width: 85,
      height: 25,
      marginTop: 4,
    },
    "@media (max-width: 320px)": {
      width: 70,
      height: 20,
      fontSize: 12,
      marginTop: 2,
    },
  },
});
const ListItem = () => {
  const classes = useStyles();
  const [items, setItem] = useState([]);
  const productId = useParams();
  const [isLoading, setIsLoading] = React.useState(true);

  useEffect(() => {
    const getData = async () => {
      setIsLoading(true);
      await SetupAPI.get(
        `/customer/ecommerce/products?category=${productId.id}&page=1`
      )
        .then((res) => {
          setItem(res.data.data);
        })
        .catch((err) => {
          console.log(err);
        })
        .finally(() => {
          setIsLoading(false);
        });
    };
    getData();
  }, [productId]);

  return (
    <ThemeProvider theme={theme}>
      {isLoading ? (
        <div
          style={{
            display: "flex",
            alignItems: "center",
            flexDirection: "column",
            justifyContent: "center",
            minHeight: "80vh",
            margin: 0,
          }}
        >
          <CircularProgress
            style={{
              color: "#00A739",
            }}
          />
        </div>
      ) : (
        <>
          <Grid container className={classes.GridContain}>
            <div style={{ width: "100%" }}>
              <InfiniteScroll dataLength={{ item: Array.from({ length: 20 }) }}>
                {items.map((item, index) => (
                  <Grid
                    item={true}
                    xs={12}
                    style={{
                      padding: "0px 16px",
                      backgroundColor: "rgb(250, 250, 250)",
                    }}
                    key={index}
                  >
                    <Box className={classes.BoxCard}>
                      <Grid className={classes.GridCard} container>
                        <Grid item={true} xs={4}>
                          <CardMedia
                            className={classes.GridImage}
                            style={{
                              filter: "unset",
                            }}
                            image={item.image.url}
                          >
                            {item.isPromo === true ? (
                              <div
                                className={classes.DiscountBadge}
                                style={{ justifyContent: "flex-start" }}
                              >
                                <p className={classes.DiscountText}>
                                  Disk.{" "}
                                  {Math.floor(
                                    (100 *
                                      (item.regularPrice - item.promoPrice)) /
                                      item.regularPrice
                                  )}
                                  %
                                </p>
                              </div>
                            ) : (
                              <div className={classes.DiscountBadge} />
                            )}
                          </CardMedia>
                        </Grid>
                        <Grid item={true} xs={8} style={{ paddingLeft: 10 }}>
                          <p className={classes.Title}>{item.name}</p>
                          <div
                            style={{
                              fontSize: 10,
                              color: "rgb(204, 204, 204)",
                            }}
                          >
                            Min. order 1 x {item.unit}
                          </div>
                          <div className={classes.PriceUnit}>
                            <div
                              style={{ display: "flex", alignItems: "center" }}
                            >
                              <Typography
                                className={classes.Price}
                                variant="body1"
                              >
                                {CurrencyFormatter.format(item.price).replace(
                                  "IDR",
                                  ""
                                )}
                              </Typography>
                              <Typography
                                className={classes.Unit}
                                variant="body1"
                              >
                                /{item.unit}
                              </Typography>
                            </div>
                          </div>
                          <div
                            style={{
                              display: "flex",
                              alignItems: "flex-end",
                              justifyContent: "flex-end",
                            }}
                          >
                            <div>
                              <Button
                                className={classes.BuyButton}
                                variant="text"
                                style={{
                                  color: "rgb(255, 255, 255)",
                                  backgroundColor: "rgb(0, 167, 57)",
                                }}
                              >
                                Beli
                              </Button>
                            </div>
                          </div>
                        </Grid>
                      </Grid>
                    </Box>
                  </Grid>
                ))}
              </InfiniteScroll>
            </div>
          </Grid>
        </>
      )}
    </ThemeProvider>
  );
};

export default ListItem;
