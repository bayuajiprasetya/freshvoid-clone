import { Paper, makeStyles, Grid, Typography } from "@material-ui/core";
import React, { useContext } from "react";
import { CartContext } from "../context/Cart";
import { CartIcon } from "../Icon/Icons";
import CurrencyFormatter from "./../utils/Currency";

const styles = {
  Snackbar: {
    display: "flex",
    justifyContent: "flex-end",
  },

  Paper: {
    width: "100%",
    maxWidth: 442,
    position: "fixed",
    height: 70,
    bottom: 70,
    background: "transparent",
    borderRadius: 0,
    padding: "0px 16px 16px",
    boxShadow: "none",
    border: "none",
    color: "rgb(255, 255, 255)",
    filter: "unset",
  },

  Icon: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    marginRight: 10,
  },

  TotalPriceBox: {
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-end",
    justifyContent: "center",
    fontWeight: "bold",
    color: "rgb(255, 255, 255)",
  },

  TotalItem: {
    marginBottom: -5,
    fontWeight: "bold",
    fontSize: 14,
    display: "flex",
    alignItems: "center",
    color: "rgb(255, 255, 255)",
  },

  LocationText: {
    fontSize: 12,
    color: "rgb(255, 255, 255)",
  },
};

const useStyles = makeStyles({
  PaperContainer: {
    color: "#ffffff",
    width: "100%",
    maxHeight: 100,
    borderRadius: 5,
    backgroundColor: "#00A739",
  },
});

const Snackbar = () => {
  const classes = useStyles();
  const { cart, price } = useContext(CartContext);

  return (
    <div style={styles.Snackbar}>
      {cart.length > 0 && (
        <Paper elevation={1} style={styles.Paper}>
          <Paper
            elevation={0}
            className={classes.PaperContainer}
            style={{ marginBottom: 41 }}
          >
            <Grid container style={{ padding: "8px 16px" }}>
              <Grid item xs={1} style={styles.Icon}>
                <CartIcon />
              </Grid>
              <Grid item xs={6}>
                <Typography
                  variant="caption"
                  gutterBottom
                  display="block"
                  style={styles.TotalItem}
                >
                  {cart.length} Item
                </Typography>
                <Typography variant="caption" style={styles.LocationText}>
                  Freshvo Cilandak (Jabodetabek)
                </Typography>
              </Grid>
              <Grid item xs={4} style={styles.TotalPriceBox}>
                {CurrencyFormatter.format(price).replace("IDR", "")}
              </Grid>
            </Grid>
          </Paper>
        </Paper>
      )}
    </div>
  );
};

export default Snackbar;
