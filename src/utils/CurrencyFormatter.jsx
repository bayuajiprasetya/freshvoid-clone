const CurrencyFormatter = new Intl.NumberFormat("id-ID", {
  minimumFractionDigits: 0,
});
export default CurrencyFormatter;
