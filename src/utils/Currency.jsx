const CurrencyFormatter = new Intl.NumberFormat("id-ID", {
  style: "currency",
  currency: "IDR",
  currencyDisplay: "code",
  minimumFractionDigits: 0,
});
export default CurrencyFormatter;
