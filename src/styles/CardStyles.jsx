import { makeStyles } from "@material-ui/core";

const styles = {
  GridText: {
    display: "flex",
    flexDirection: "row",
    padding: "16px 16px 20px",
  },

  Title: {
    fontSize: 13,
    fontWeight: 700,
  },

  SubTitle: {
    fontSize: 10,
    fontWeight: 400,
    color: "rgb(37, 37, 37)",
  },

  AnchorSeeAll: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
  },

  CardInfoContain: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
    height: "100%",
  },

  DiscountStyles: {
    justiyContent: "flex-start",
  },

  CardMinOrderInfo: {
    fontSize: 10,
    color: "rgb(204, 204, 204)",
  },

  CardItemPrice: {
    justifyContent: "center",
    height: "100%",
  },

  Price: {
    height: 35,
    display: "flex",
    flexDirection: "unset",
  },

  BuyButtonStyles: {
    color: "rgb(255, 255, 255)",
    backgroundColor: "rgb(0, 167, 57)",
  },

  ButtonGroup: {
    justifyContent: "flex-end",
    width: "-webkit-fill-available",
    marginRight: 1,
  },
};

const useStyles = makeStyles({
  root: {
    margin: "8px 0 0",
    minHeight: 390,
    paddingBottom: 16,
    backgroundColor: "#fff",
  },

  AnchorSeeAll: {
    color: "#2F9CF1",
    cursor: "pointer",
    fontSize: 10,
    fontWeight: "bold",
    "@media (max-width: 375px)": {
      fontSize: "9px !important",
    },
  },

  Cards: {
    height: "110%",
    display: "flex",
    padding: "8px 16px",
    flexWrap: "nowrap",
    overflowX: "auto",
    backgroundColor: "#fff",
    WebkitOverflowScrolling: "touch",
    "&::-webkit-scrollbar": {
      height: 1,
    },
    "&::-webkit-scrollbar-thumb": {
      backgroundColor: "transparent",
    },
  },

  Card: {
    cursor: "pointer",
    display: "flex",
    minWidth: 150,
    boxShadow: "0px 4px 10px rgb(0 0 0 / 5%)",
    marginRight: 16,
    borderRadius: 7,
    flexDirection: "column",
  },

  SpaceGrid: { flex: "0 0 auto", width: 1 },

  CardImage: {
    width: 150,
    height: 140,
    objectFit: "cover",
    borderRadius: "7px 7px 0 0",
  },

  CardDiscount: {
    height: "100%",
    display: "flex",
    flexDirection: "column",
  },

  DiscountText: {
    color: "#ffffff",
    width: "fit-content",
    margin: 0,
    opacity: 0.8,
    padding: 8,
    fontSize: 10,
    textAlign: "center",
    fontWeight: "bold",
    borderRadius: "5px 0px 5px 0px",
    backgroundColor: "#00A739",
  },

  CardInfo: {
    width: 150,
    display: "flex",
    padding: 16,
    objectFit: "cover",
    borderRadius: "0 0 7px 7px",
    flexDirection: "column",
    justifyContent: "space-between",
  },

  CardItemTitle: {
    height: 30,
    overflow: "hidden",
    fontSize: 14,
    fontWeight: 600,
    whiteSpace: "nowrap",
    textOverflow: "ellipsis",
  },

  StrikeNormalPrice: {
    color: "#252525",
    fontSize: 10,
  },

  NumberPrice: {
    color: "#252828",
    fontSize: 13,
    fontWeight: "bold",
  },

  WeightPrice: {
    fontSize: 10,
    marginLeft: 2,
  },

  BuyButton: {
    color: "#ffffff",
    width: "100%",
    height: 28,
    display: "flex",
    fontSize: "13px !important",
    alignItems: "center",
    fontFamily: `https://fonts.googleapis.com/css2?family=Open+Sans&display=swap`,
    fontWeight: 700,
    borderRadius: 5,
    textTransform: "none",
    justifyContent: "center",
    backgroundColor: "#00A739",
  },
});

export { styles, useStyles };
