import { makeStyles } from "@material-ui/core";

const styles = {
  root: {
    position: "fixed",
    maxWidth: 442,
    top: 0,
    zIndex: 99,
    height: 150,
    width: "100%",
    backgroundColor: "rgb(255, 255, 255)",
    boxShadow: "rgba(0, 0, 0, 0.1) 0px 4px 4px",
  },

  Center: { display: "flex", flexDirection: "row", marginTop: 6 },

  StoreIcon: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },

  CenterName: { fontSize: 11, fontWeight: 700, marginLeft: 8 },

  BoxChangeLocation: {
    display: "flex",
    alignItems: "center",
    cursor: "pointer",
  },

  ChangeLocationText: {
    fontSize: 10,
    fontWeight: 500,
    color: "rgb(47, 156, 241)",
    marginRight: 6,
  },
};

const useStyles = makeStyles({
  NavbarFlex: {
    width: "100%",
    height: 150,
    padding: 16,
    backgroundColor: "#fff",
  },

  LocationInfo: {
    width: "100%",
    display: "flex",
    flexDirection: "row",
  },

  LocationText: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-end",
  },

  InformationText: {
    color: "#4E5356",
    fontSize: 9,
    fontWeight: 600,
  },

  ChangeLocation: {
    display: "flex",
    alignItems: "flex-end",
    flexDirection: "row",
    justifyContent: "flex-end",
  },

  SearchInput: {
    width: "100%",
    height: 50,
    display: "flex",
    padding: "8px 16px",
    marginTop: 16,
    alignItems: "center",
    borderRadius: 100,
    backgroundColor: "#F1F2F6",
  },

  SearchIcon: {
    color: "#707585",
    display: "flex",
    position: "absolute",
    alignItems: "center",
    pointerEvents: "none",
    justifyContent: "center",
  },

  InputBase: {
    width: "100%",
    height: "100%",
    marginLeft: 32,
    color: "rgb(112, 117, 133)",
    "& .MuiInputBase-input": {
      width: "100%",
      fontSize: "12px !important",
      fontWeight: 500,
    },
  },
});

export { styles, useStyles };
