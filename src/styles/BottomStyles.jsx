import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles({
  root: {
    display: "flex",
    boxShadow:
      "rgb(0 0 0 / 20%) 0px 2px 1px -1px, rgb(0 0 0 / 14%) 0px 1px 1px 0px, rgb(0 0 0 / 12%) 0px 1px 3px 0px",
    justifyContent: "center",
    backgroundColor: "rgb(255, 255, 255)",
  },

  Grid: {
    width: "100%",
    border: "0px solid #e0e0e0",
    bottom: 0,
    zIndex: 1000,
    position: "fixed",
    maxWidth: 442,
    boxShadow: "0px 0px 2px #9e9e9e",
  },
  BottomIcon: {
    fill: "#a3a3a3",
    color: "#a3a3a3",
    width: "100%",
  },
});

export { useStyles };
