import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles({
  root: {
    width: "100%",
    padding: 16,
    marginTop: 8,
    backgroundColor: "#fff",
  },

  Contain: {
    display: "flex",
    flexWrap: "wrap",
    alignItems: "flex-start",
    flexDecoration: "row",
    justifyContent: "flex-start",
  },

  ItemGrid: {
    cursor: "pointer",
    display: "flex",
    textAlign: "center",
    alignItems: "center",
    marginBottom: 6,
    flexDirection: "column",
  },

  CategoryIcon: {
    width: 64,
    border: "1px solid #E8E8E8",
    height: 64,
    display: "flex",
    padding: 10,
    transition: "all 0.3s ease 0s",
    alignItems: "center",
    marginLeft: 8,
    marginRight: 8,
    borderRadius: 10,
    marginBottom: 6,
    justifyContent: "center",
    backgroundColor: "white",
    "@media (max-width: 443px)": {
      width: 50,
      height: 50,
      fontSize: 10,
      marginLeft: 8,
      marginRight: 8,
    },
    "@media (max-width: 388px)": {
      width: 50,
      height: 50,
      fontSize: 9,
    },
    "&:hover": {
      boxShadow: "0px 4px 4px rgb(0 0 0 / 25%)",
    },
  },

  CategoryImage: {
    width: "100%",
    height: "100%",
  },

  CategoryText: {
    color: "#707585",
    margin: "0 5px",
    fontSize: 13,
    textAlign: "center",
    fontWeight: 500,
    "@media (max-width: 440px)": {
      fontSize: 10,
    },
    "@media (max-width: 375px)": {
      fontSize: 8,
    },
  },
});

export { useStyles };
