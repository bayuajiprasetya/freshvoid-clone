import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles({
  CarouselPaper: {
    width: "100%",
    backgroundColor: "#fff",
  },

  CarouselContain: {
    width: "100%",
    display: "flex",
    padding: 16,
    flexDirection: "column",
  },
});

export { useStyles };
