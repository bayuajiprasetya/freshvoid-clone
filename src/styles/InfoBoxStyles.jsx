import { makeStyles } from "@material-ui/core";

const styles = {
  root: { marginTop: 130, display: "block" },

  RewardVoucherBox: {
    display: "flex",
    width: "50%",
    borderRight: "1px solid rgb(231, 231, 231)",
    padding: "0px 12px",
  },

  RewardPoinBox: { display: "flex", paddingLeft: 12 },
};

const useStyles = makeStyles({
  root: {
    width: "100%",
    padding: 16,
    marginTop: 8,
    backgroundColor: "#fff",
  },

  InfoContain: {
    display: "flex",
    padding: "12px 0px",
    boxShadow: "0px 2px 8px rgb(0 0 0 / 4%)",
    borderRadius: 8,
    backgroundColor: "#ffffff",
  },

  InfoRewardText: {
    color: "#808080",
    fontSize: 12,
    fontWeight: 500,
  },

  AnchorReward: {
    color: "#00A739",
    cursor: "pointer",
    fontSize: 14,
    marginTop: 3,
    fontWeight: 500,
  },
});

export { styles, useStyles };
