import React from "react";
import { Grid, SvgIcon, Typography, InputBase } from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";
import { styles, useStyles } from "./../styles/NavbarStyles";
import ChangeLocation from "../assets/Change Location.svg";
import { FreshvoIcon } from "../Icon/Icons";

const Navbar = () => {
  const classes = useStyles();

  return (
    <div style={styles.root}>
      <Grid className={classes.NavbarFlex} container={true} item={true} xs={12}>
        <div className={classes.LocationInfo}>
          <Grid className={classes.LocationText} item={true} xs={9}>
            <div>
              <Typography className={classes.InformationText} variant="body1">
                Kamu berbelanja di
              </Typography>
            </div>
            <div style={styles.Center}>
              <div style={styles.StoreIcon}>
                <FreshvoIcon />
              </div>
              <Typography variant="body1" style={styles.CenterName}>
                Freshvo Cilandak (Jabodetabek)
              </Typography>
            </div>
          </Grid>
          <Grid className={classes.ChangeLocation} item={true} xs={3}>
            <div style={styles.BoxChangeLocation} path="/market">
              <Typography variant="body1" style={styles.ChangeLocationText}>
                Ubah
              </Typography>
              <img src={ChangeLocation} alt="Ganti Lokasi" />
            </div>
          </Grid>
        </div>
        <div className={classes.SearchInput}>
          <div className={classes.SearchIcon}>
            <SvgIcon focusable="false" viewBox="0 0 24 24" aria-hidden="true">
              <SearchIcon />
            </SvgIcon>
          </div>
          <InputBase placeholder="Cari produk." className={classes.InputBase} />
        </div>
      </Grid>
    </div>
  );
};

export default Navbar;
