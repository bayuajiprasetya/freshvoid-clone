import React, { useEffect } from "react";
import {
  BottomNavigation,
  BottomNavigationAction,
  Box,
  createTheme,
  ThemeProvider,
} from "@material-ui/core";
import {
  ProfilIcon,
  BantuanIcon,
  TransaksiIcon,
  BelanjaIcon,
} from "../Icon/Icons";
import { useStyles } from "../styles/BottomStyles";
import { useHistory } from "react-router-dom";

const navTheme = createTheme({
  palette: {
    primary: {
      main: "#00A739",
    },
  },
});

const BottomMenu = () => {
  const classes = useStyles();
  const [value, setValue] = React.useState("");
  const history = useHistory();

  const handleChange = (event, newValue) => {
    history.push(`/${newValue}`);
    setValue(newValue);
  };

  useEffect(() => {
    if (window.location.pathname === "/") {
      setValue("");
    } else if (window.location.pathname === "/transaction") {
      setValue("transaction");
    } else if (window.location.pathname === "/help") {
      setValue("help");
    } else if (window.location.pathname === "/profile") {
      setValue("profile");
    }
  }, []);

  return (
    <ThemeProvider theme={navTheme}>
      <Box className={classes.root}>
        <BottomNavigation
          className={classes.Grid}
          showLabels
          value={value}
          onChange={handleChange}
        >
          <BottomNavigationAction
            className={classes.BottomIcon}
            value=""
            to="/"
            label={<b style={{ fontSize: 11 }}>Belanja</b>}
            icon={<BelanjaIcon />}
          />
          <BottomNavigationAction
            value="transaction"
            to="/transaction"
            className={classes.BottomIcon}
            label={<b style={{ fontSize: 11 }}>Transaksi</b>}
            icon={<TransaksiIcon />}
          />
          <BottomNavigationAction
            value="help"
            to="/help"
            className={classes.BottomIcon}
            label={<b style={{ fontSize: 11 }}>Bantuan</b>}
            icon={<BantuanIcon />}
          />
          <BottomNavigationAction
            value="profile"
            to="/profile"
            className={classes.BottomIcon}
            label={<b style={{ fontSize: 11 }}>Profil</b>}
            icon={<ProfilIcon />}
          />
        </BottomNavigation>
      </Box>
    </ThemeProvider>
  );
};

export default BottomMenu;
