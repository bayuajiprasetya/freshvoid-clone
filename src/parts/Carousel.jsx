import React from "react";
import { Grid, Typography } from "@material-ui/core";
import { useStyles } from "../styles/CarouselStyles";
import Autoplay from "../components/Autoplay";

const Carousel = () => {
  const classes = useStyles();

  return (
    <div className={classes.CarouselPaper} style={{ marginTop: 0 }}>
      <Grid
        className={classes.CarouselContain}
        container={true}
        item={true}
        xs={12}
      >
        <Typography variant="body1" style={{ fontSize: 13, fontWeight: 700 }}>
          Informasi Terbaru
        </Typography>
        <Typography
          variant="body1"
          style={{ fontSize: 10, fontWeight: 400, color: "rgb(82, 87, 92)" }}
        >
          Informasi terbaru hanya untukmu!
        </Typography>
        <div style={{ marginTop: 12 }}>
          <Autoplay />
        </div>
      </Grid>
    </div>
  );
};

export default Carousel;
