import { Grid } from "@material-ui/core";
import React, { useState, useEffect } from "react";
import { useStyles } from "../styles/CategoryStyles";
import SetupAPI from "../AxiosInstance";
import { useHistory } from "react-router-dom";

const Category = () => {
  const classes = useStyles();
  const [categories, setCategories] = useState([]);
  const history = useHistory();

  useEffect(() => {
    const getData = async () => {
      await SetupAPI.get(
        `/customer/ecommerce/products/categories?page=1&perPage=100&isParent=true`
      )
        .then((res) => {
          setCategories(res.data.data);
        })
        .catch((err) => {
          console.log(err);
        });
    };
    getData();
  }, []);

  return (
    <div className={classes.root}>
      <div className={classes.Contain}>
        {categories.map((category) => (
          <div
            style={{ width: "20%" }}
            key={category.id}
            onClick={() => history.push(`/category/${category.id}`)}
          >
            <span>
              <Grid className={classes.ItemGrid} item={true}>
                <div className={classes.CategoryIcon}>
                  <img
                    src={category.image.url}
                    alt={category.name}
                    className={classes.CategoryImage}
                  />
                </div>
                <span className={classes.CategoryText}>{category.name}</span>
              </Grid>
            </span>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Category;
