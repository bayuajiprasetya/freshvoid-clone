import { Grid, Typography, CardMedia } from "@material-ui/core";
import React, { useState, useEffect } from "react";
import { styles, useStyles } from "../styles/CardStyles";
import SetupAPI from "../AxiosInstance";
import CurrencyFormatter from "./../utils/CurrencyFormatter";
import ButtonAdd from "../components/ButtonAdd";
import { useHistory } from "react-router-dom";

const Card = () => {
  const classes = useStyles();
  const [cards, setCards] = useState([]);
  const history = useHistory();

  useEffect(() => {
    const getData = async () => {
      await SetupAPI.get(`/customer/ecommerce/products?isFeatured=true&page=1`)
        .then((res) => {
          setCards(res.data.data);
        })
        .catch((err) => {
          console.log(err);
        });
    };
    getData();
  }, []);

  return (
    <div className={classes.root}>
      <Grid item={true} xs={12} style={styles.GridText}>
        <Grid item={true} xs={9}>
          <Typography variant="body1" style={styles.Title}>
            Produk Pilihan
          </Typography>
          <Typography variant="body1" style={styles.SubTitle}>
            Produk pilihan terbaik, hanya untukmu!
          </Typography>
        </Grid>

        <Grid item={true} xs={3} style={styles.AnchorSeeAll}>
          <span
            className={classes.AnchorSeeAll}
            onClick={() => history.push("/top-seller")}
          >
            Lihat Semua
          </span>
        </Grid>
      </Grid>

      <Grid className={classes.Cards}>
        {cards.map((item, index) => (
          <div className={classes.Card} key={index}>
            <CardMedia
              className={classes.CardImage}
              style={{
                filter: "unset",
              }}
              image={item.image.url}
              onClick={() => history.push(`/product/${item.id}`)}
            >
              {item.isPromo === true ? (
                <div className={classes.CardDiscount}>
                  <p className={classes.DiscountText}>
                    Disk.{" "}
                    {Math.floor(
                      (100 * (item.regularPrice - item.promoPrice)) /
                        item.regularPrice
                    )}
                    %
                  </p>
                </div>
              ) : null}
            </CardMedia>
            <div className={classes.CardInfo}>
              <div
                style={styles.CardInfoContain}
                onClick={() => history.push(`/product/${item.id}`)}
              >
                <div className={classes.CardItemTitle}>{item.name}</div>
                <div style={styles.CardMinOrderInfo}>
                  Min. order {item.minimumOrderQuantity} x {item.unit}
                </div>
                <div style={styles.CardItemPrice}>
                  {item.isPromo === true ? (
                    <strike className={classes.StrikeNormalPrice}>
                      {CurrencyFormatter.format(item.regularPrice).replace(
                        "IDR",
                        ""
                      )}
                    </strike>
                  ) : (
                    <strike />
                  )}
                  <br />
                  <div style={styles.Price}>
                    <Typography className={classes.NumberPrice} variant="body1">
                      {CurrencyFormatter.format(item.price).replace("IDR", "")}
                    </Typography>
                    <Typography className={classes.WeightPrice} variant="body1">
                      /{item.unit}
                    </Typography>
                  </div>
                </div>
              </div>
              <div>
                <ButtonAdd data={item} />
              </div>
            </div>
          </div>
        ))}
        <Grid className={classes.SpaceGrid} />
      </Grid>
    </div>
  );
};

export default Card;
