import {
  AddressIcon,
  PromoCodeIcon,
  TickerVoucherIcon,
  TransaksiIcon,
  WalletIcon,
} from "../Icon/Icons";

const contain = [
  {
    id: 1,
    text: "Pesanan Kamu",
    icon: <TransaksiIcon />,
    path: "/transaction",
  },
  {
    id: 2,
    text: "Alamat Tersimpan",
    icon: <AddressIcon />,
  },
  {
    id: 3,
    text: "Poin Kamu",
    icon: <WalletIcon />,
    path: "/profile/point",
  },
  {
    id: 4,
    text: "Voucher Kamu",
    icon: <TickerVoucherIcon />,
    path: "/profile/vouchers",
  },
  {
    id: 5,
    text: "Masukkan Kode Promo",
    icon: <PromoCodeIcon />,
  },
];

export default contain;
